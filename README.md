# proxy swarm

this library fetches some fresh http proxy addresses from http://www.gatherproxy.com/ and sets up an Apache Http Client using randomly one of the proxies. 

the library holds a number of proxies (the pool) in memory, processes requests according to your needs and exchanges bad proxy hosts in the pool based on request success statistics. 

## use as a maven dependency
u may use the library through jitpack:
```xml
	<repositories>
		<repository>
		    <id>jitpack.io</id>
		    <url>https://jitpack.io</url>
		</repository>
	</repositories>

	<dependencies>
		<dependency>
		    <groupId>com.gitlab.colawilde</groupId>
		    <artifactId>proxy-swarm</artifactId>
		    <version>e0bb114e</version>
		</dependency>	
	</dependencies>  
```

## configuration and usage

```java
    // define a timeout for everything within one request
	int timeout = 10;
    
    // setup a request config
	RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout * 1000)
		.setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(timeout * 1000).build();

    // create a http client
	CloseableHttpClient defaultHttpClient = HttpClientBuilder.create()
		.setRetryHandler(new HttpRequestRetryHandler() {

		    @Override
		    public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
			if (executionCount >= 1) {
			    return false;
			}
			return true;
		    }

		}).useSystemProperties().setDefaultRequestConfig(config).build();
    
    // create a proxy swarm configuration
	ProxySwarmConfig proxySwarmConfig = new ProxySwarmConfig();
	
	// number of proxies we want to use (dont oversize it)
	proxySwarmConfig.setTargetProxyPoolSize(4);
	
	// after that time a pending request gets killed and a retry will be initiated
	proxySwarmConfig.setMaxProcessingTimePerRequest(10000l);
	
	// here are some proxy list fetchers defined, 
	// responsible to get the proxy definitions
	proxySwarmConfig.setProxyListFetchers(Arrays
		.asList(new ProxyListFetcher[] { new POSTGatherProxyListFetcher(), new GatherProxyListFetcher() }));
		
	// initialize the ProxySwarm
	ProxySwarm proxySwarm = new ProxySwarm(proxySwarmConfig, defaultHttpClient);

    // now you can use the proxy swarm with a Apache http get or post: 

	HttpGet httpGetRequest = new HttpGet(url);
	httpGetRequest.setConfig(config);
	    
	HttpResponse httpResponse = proxySwarm.createRequest(httpGetRequest);

```



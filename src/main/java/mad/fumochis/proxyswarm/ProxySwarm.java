package mad.fumochis.proxyswarm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mad.fumochis.proxyswarm.fetcher.ProxyListFetcher;
import mad.fumochis.proxyswarm.model.ProxyDefinition;
import mad.fumochis.proxyswarm.model.ProxyStats;
import mad.fumochis.proxyswarm.model.ProxySwarmConfig;

public class ProxySwarm {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProxySwarm.class);

    public static final String PROXY_SWARM_HEADER = "PROXY_SWARM_ADDRESS";
    final ProxySwarmConfig proxySwarmConfig;
    final List<ProxyDefinition> proxyList = new ArrayList<>();
    final Random randomGenerator = new Random();
    final CloseableHttpClient defaultHttpClient;

    public ProxySwarm(ProxySwarmConfig proxySwarmConfig, CloseableHttpClient defaultHttpClient) {
	super();
	this.proxySwarmConfig = proxySwarmConfig;
	this.defaultHttpClient = defaultHttpClient;
	populateProxyDefinitionList();
    }

    private void populateProxyDefinitionList() {

	int neededProxiesNum = proxySwarmConfig.getTargetProxyPoolSize() - getNumGoodProxiesInList();
	while (neededProxiesNum > 0) {
	    try {
		LOGGER.info("trying to populate pool with " + neededProxiesNum + " additional proxies");
		ProxyListFetcher proxyListFetcher = getRandomProxyListProvider();
		List<ProxyDefinition> newlyFetchedProxies = proxyListFetcher.getProxies(neededProxiesNum);
		mergeNewProxiesIntoList(newlyFetchedProxies);
	    } catch (IOException e) {
		LOGGER.warn("cannot fetch proxies with " + ProxyListFetcher.class.getName());
		try {
		    Thread.sleep(1000l);
		} catch (InterruptedException e1) {
		    LOGGER.warn("sleep was interrupted");
		}
	    }
	    neededProxiesNum = proxySwarmConfig.getTargetProxyPoolSize() - getNumGoodProxiesInList();
	}
    }

    private void mergeNewProxiesIntoList(List<ProxyDefinition> newlyFetchedProxies) {
	for (ProxyDefinition newProxy : newlyFetchedProxies) {
	    if (!proxyList.contains(newProxy)
		    && getNumGoodProxiesInList() <= proxySwarmConfig.getTargetProxyPoolSize()) {
		proxyList.add(newProxy);
		LOGGER.info("added new proxy: " + newProxy + ":" + getNumGoodProxiesInList());
	    }
	}
    }

    public void cleanupProxies() {
	for (ProxyDefinition proxyDef : proxyList) {
	    if (proxyDef.getProxyStats().getSuccessRate() <= (ProxyStats.statsRequestLogSize / 2)) {
		proxyDef.setBlacklisted(true);
	    }
	}
	populateProxyDefinitionList();
    }

    public HttpResponse createRequest(HttpRequestBase request) {

	int statusCode = -1;

	int retryCount = 0;

	HttpResponse httpResponse = null;

	while (statusCode != 200 && retryCount < 10) {

	    ProxyDefinition randomProxy = getRandomProxyDefinition();
	    RequestConfig proxyRequestConfig = RequestConfig.copy(request.getConfig())
		    .setProxy(new HttpHost(randomProxy.getIp(), randomProxy.getPort())).build();
	    request.setConfig(proxyRequestConfig);
	    ExecutorService executor = Executors.newFixedThreadPool(1);

	    long requestTime = System.currentTimeMillis();

	    try {

		LOGGER.info("executing request");
		randomProxy.getProxyStats().increaseUsedCount();
		randomProxy.getProxyStats().getLastResponseCodes().put(requestTime, Boolean.FALSE);
		statusCode = 0;

		Future<HttpResponse> future = null;
		future = executor.submit(new Callable<HttpResponse>() {
		    @Override
		    public HttpResponse call() throws ClientProtocolException, IOException {

			return defaultHttpClient.execute(request);
		    }
		});

		httpResponse = future.get(proxySwarmConfig.getMaxProcessingTimePerRequest(), TimeUnit.MILLISECONDS);

		LOGGER.debug("executed request");
		statusCode = httpResponse.getStatusLine().getStatusCode();
		LOGGER.debug("----------------------------------------");
		LOGGER.debug("" + httpResponse.getStatusLine());
		LOGGER.debug("----------------------------------------");

		if (statusCode == 200) {
		    try {
			httpResponse.setEntity(new BufferedHttpEntity(httpResponse.getEntity()));
			httpResponse.setHeader(PROXY_SWARM_HEADER, randomProxy.getIp() + ":" + randomProxy.getPort());
		    } catch (IOException e) {
			LOGGER.error("something went wrong buffering the entity in the response", e);
		    }
		}

	    }

	    catch (InterruptedException e) {

		LOGGER.warn("task interrupted");
	    } catch (ExecutionException e) {

		LOGGER.error(" execution exception", e);
	    } catch (TimeoutException e) {

		LOGGER.debug("future timed out");
	    }

	    finally {
		request.releaseConnection();
		executor.shutdown();
	    }

	    if (statusCode == 200) {
		randomProxy.getProxyStats().getLastResponseCodes().put(requestTime, Boolean.TRUE);

		randomProxy.getProxyStats().increaseSuccessCount();
		randomProxy.getProxyStats().setLastUsedTimestamp(System.currentTimeMillis());

	    }

	    retryCount++;

	}

	cleanupProxies();

	return httpResponse;

    }

    private ProxyListFetcher getRandomProxyListProvider() {

	int index = randomGenerator.nextInt(proxySwarmConfig.getProxyListFetchers().size());
	return proxySwarmConfig.getProxyListFetchers().get(index);

    }

    private ProxyDefinition getRandomProxyDefinition() {

	ProxyDefinition proxyDef = new ProxyDefinition();
	proxyDef.setBlacklisted(true);
	while (proxyDef.isBlacklisted()) {
	    int index = randomGenerator.nextInt(proxyList.size());
	    proxyDef = proxyList.get(index);
	}
	return proxyDef;

    }

    private int getNumGoodProxiesInList() {
	int goodProxies = 0;
	for (ProxyDefinition proxyDef : proxyList) {
	    if (!proxyDef.isBlacklisted()) {
		goodProxies++;
	    }
	}
	return goodProxies;
    }

    public void getProxyStats() {
	for (ProxyDefinition proxyDef : proxyList) {

	    LOGGER.info("proxyDef=" + proxyDef.getProxyStats().getSuccessRate() + ":" + proxyDef);

	}
    }

}

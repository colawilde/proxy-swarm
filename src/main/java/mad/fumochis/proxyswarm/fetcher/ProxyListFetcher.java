package mad.fumochis.proxyswarm.fetcher;

import java.io.IOException;
import java.util.List;

import mad.fumochis.proxyswarm.model.ProxyDefinition;

public interface ProxyListFetcher {

    List<ProxyDefinition> getProxies(int num) throws IOException;

}

package mad.fumochis.proxyswarm.fetcher.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import mad.fumochis.proxyswarm.fetcher.ProxyListFetcher;
import mad.fumochis.proxyswarm.model.ProxyDefinition;

public class POSTGatherProxyListFetcher implements ProxyListFetcher {

    final Random randomGenerator = new Random();

    private static final Logger LOGGER = LoggerFactory.getLogger(POSTGatherProxyListFetcher.class);
    private static final String url = "http://www.gatherproxy.com/proxylist/anonymity/?t=Elite";

    @Override
    public List<ProxyDefinition> getProxies(int num) throws IOException {

	// HttpGet request = null;
	HttpPost request = null;

	List<ProxyDefinition> proxyList = new ArrayList<>();
	ObjectMapper om = new ObjectMapper();

	try {

	    HttpClient client = HttpClientBuilder.create().build();

	    request = new HttpPost(url);
	    request.setHeader("User-Agent",
		    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("Type", "elite"));

	    int index = randomGenerator.nextInt(20);

	    params.add(new BasicNameValuePair("PageIdx", new String("" + index)));
	    params.add(new BasicNameValuePair("Uptime", "0"));
	    request.setEntity(new UrlEncodedFormEntity(params));

	    HttpResponse response = client.execute(request);

	    String inputLine;
	    BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	    try {

		ProxyDefinition proxyDefinition = new ProxyDefinition();

		while ((inputLine = br.readLine()) != null) {

		    Matcher matcher1 = Pattern.compile("document.write\\('(.*)'\\)").matcher(inputLine);
		    Matcher matcher2 = Pattern.compile("document.write\\(gp.dep\\('(.*)'\\)").matcher(inputLine);

		    if (matcher1.find()) {
			proxyDefinition.setIp(matcher1.group(1));
		    }
		    if (matcher2.find()) {
			proxyDefinition.setPort(Integer.parseInt(matcher2.group(1), 16));
		    }

		    if (proxyDefinition.getPort() != null) {
			proxyList.add(proxyDefinition);
			LOGGER.info("found " + proxyDefinition);
			proxyDefinition = new ProxyDefinition();
		    }

		}
		br.close();
	    } catch (IOException e) {
		throw e;
	    }

	} finally {

	    if (request != null) {

		request.releaseConnection();
	    }
	}
	return proxyList;
    }
}

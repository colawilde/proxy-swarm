package mad.fumochis.proxyswarm.fetcher.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import mad.fumochis.proxyswarm.fetcher.ProxyListFetcher;
import mad.fumochis.proxyswarm.model.GatherProxyObj;
import mad.fumochis.proxyswarm.model.ProxyDefinition;

public class GatherProxyListFetcher implements ProxyListFetcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(GatherProxyListFetcher.class);

    // private static final String url =
    // "http://www.gatherproxy.com/proxylist/anonymity/?t=Transparent";
    private static final String url = "http://www.gatherproxy.com/proxylist/anonymity/?t=Elite";

    @Override
    public List<ProxyDefinition> getProxies(int num) throws IOException {

	HttpGet request = null;

	List<ProxyDefinition> proxyList = new ArrayList<>();
	ObjectMapper om = new ObjectMapper();

	try {

	    HttpClient client = HttpClientBuilder.create().build();
	    request = new HttpGet(url);

	    request.setHeader("User-Agent",
		    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
	    HttpResponse response = client.execute(request);

	    String inputLine;
	    BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	    try {
		while ((inputLine = br.readLine()) != null) {

		    Matcher matcher = Pattern.compile("gp.insertPrx\\((.*)\\);").matcher(inputLine);
		    while (matcher.find()) {
			GatherProxyObj gatherProxyObj = om.readValue(matcher.group(1), GatherProxyObj.class);
			LOGGER.debug("got new proxy config: " + gatherProxyObj);
			ProxyDefinition proxyDefinition = new ProxyDefinition();
			proxyDefinition.setIp(gatherProxyObj.getIp());
			proxyDefinition.setPort(gatherProxyObj.getPort());
			proxyDefinition.setBlacklisted(false);

			proxyList.add(proxyDefinition);
		    }

		}
		br.close();
	    } catch (IOException e) {
		throw e;
	    }

	} finally {

	    if (request != null) {

		request.releaseConnection();
	    }
	}
	return proxyList;
    }
}

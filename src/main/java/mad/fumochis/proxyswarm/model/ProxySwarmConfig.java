package mad.fumochis.proxyswarm.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import mad.fumochis.proxyswarm.fetcher.ProxyListFetcher;

@Data
public class ProxySwarmConfig {

  /**
   * a class to define what kind of response is valued as a successfull request
   */
  final SuccessDefinition successDefinition = new SuccessDefinition();

  /**
   * maximum time before the request times out
   */
  long maxProcessingTimePerRequest = 10000l;

  /**
   * target number of proxies in list with good performance
   */
  int targetProxyPoolSize = 10;

  /**
   * List of proxy list fetchers
   */
  List<ProxyListFetcher> proxyListFetchers = new ArrayList<>();

}

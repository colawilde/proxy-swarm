package mad.fumochis.proxyswarm.model;

import lombok.Data;

@Data
public class RequestAttempt {

    private long timestamp;
    private String url;
    private Integer responseCode;

}

package mad.fumochis.proxyswarm.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class GatherProxyObj {

    private int tries = 0;
    private int success = 0;
    private int failure = 0;
    private int remaining = -1;
    private boolean blacklisted = false;

    private long remainingTimestamp = -1l;

    public void increaseTries() {
	tries++;
    }

    public void increaseSuccess() {
	success++;
    }

    public void increaseFailure() {
	failure++;
    }

    @JsonProperty("PROXY_CITY")
    private String city;

    @JsonProperty("PROXY_COUNTRY")
    private String country;

    @JsonProperty("PROXY_IP")
    private String ip;

    @JsonProperty("PROXY_LAST_UPDATE")
    private String lastUpdate;

    @JsonProperty("PROXY_PORT")
    private String port;

    @JsonProperty("PROXY_REFS")
    private String refs;

    @JsonProperty("PROXY_STATE")
    private String state;

    @JsonProperty("PROXY_STATUS")
    private String status;

    @JsonProperty("PROXY_TIME")
    private String time;

    @JsonProperty("PROXY_TYPE")
    private String type;

    @JsonProperty("PROXY_UID")
    private String uid;

    @JsonProperty("PROXY_UPTIMELD")
    private String uptime;

    public int getPort() {
	return Integer.parseInt(port, 16);
    }

}

package mad.fumochis.proxyswarm.model;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import lombok.Data;

@Data
public class ProxyStats {

    private int usedCount = 0;
    private int successCount = 0;
    private long firstUsedTimestamp = 0l;
    private long lastUsedTimestamp = 0l;

    public static int statsRequestLogSize = 10;

    private final Map<Long, Boolean> lastResponseCodes = new LinkedHashMap<Long, Boolean>(statsRequestLogSize) {

	@Override
	protected boolean removeEldestEntry(Entry<Long, Boolean> eldest) {

	    return size() > statsRequestLogSize;
	}
    };

    public void increaseUsedCount() {
	usedCount++;
    }

    public void recordRequestAttempt() {

    }

    public void increaseSuccessCount() {
	successCount++;
    }

    public Integer getSuccessRate() {
	int success = 0;

	if (lastResponseCodes.keySet().size() == statsRequestLogSize) {
	    for (Boolean result : lastResponseCodes.values()) {
		if (result.booleanValue()) {
		    success++;
		}
	    }

	} else {
	    return statsRequestLogSize;
	}

	return success;
    }

}

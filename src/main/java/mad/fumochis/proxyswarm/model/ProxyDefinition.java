package mad.fumochis.proxyswarm.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ProxyDefinition {

    @EqualsAndHashCode.Include
    private String ip;

    @EqualsAndHashCode.Include
    private Integer port;

    private boolean blacklisted = false;

    private String city;

    private String country;

    private String type;

    private final ProxyStats proxyStats = new ProxyStats();

}

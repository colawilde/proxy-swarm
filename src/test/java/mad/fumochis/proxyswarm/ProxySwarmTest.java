package mad.fumochis.proxyswarm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mad.fumochis.proxyswarm.fetcher.ProxyListFetcher;
import mad.fumochis.proxyswarm.fetcher.impl.GatherProxyListFetcher;
import mad.fumochis.proxyswarm.fetcher.impl.POSTGatherProxyListFetcher;
import mad.fumochis.proxyswarm.model.ProxySwarmConfig;

public class ProxySwarmTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProxySwarmTest.class);

    @Test
    @Ignore
    public void testIt() {

	int timeout = 10;
	RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout * 1000)
		.setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(timeout * 1000).build();

	CloseableHttpClient defaultHttpClient = HttpClientBuilder.create()
		.setRetryHandler(new HttpRequestRetryHandler() {

		    @Override
		    public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
			if (executionCount >= 1) {
			    return false;
			}
			return true;
		    }

		}).useSystemProperties().setDefaultRequestConfig(config).build();

	ProxySwarmConfig proxySwarmConfig = new ProxySwarmConfig();
	proxySwarmConfig.setTargetProxyPoolSize(4);
	proxySwarmConfig.setMaxProcessingTimePerRequest(10000l);
	proxySwarmConfig.setProxyListFetchers(Arrays
		.asList(new ProxyListFetcher[] { new POSTGatherProxyListFetcher(), new GatherProxyListFetcher() }));
	ProxySwarm proxySwarm = new ProxySwarm(proxySwarmConfig, defaultHttpClient);

	// String url = "http://www.whatsmyip.org/";
	// String url =
	// "https://api.stocktwits.com/api/2/streams/user/The_Real_Fly.json";
	String url = "https://api.iextrading.com/1.0/stock/SPY/chart/1d/";

	for (int i = 0; i < 100; i++) {
	    HttpGet httpGetRequest = new HttpGet(url);
	    httpGetRequest.setConfig(config);
	    HttpResponse httpResponse = proxySwarm.createRequest(httpGetRequest);

	    HttpEntity entity = httpResponse.getEntity();
	    try {
		String theString = EntityUtils.toString(entity, "UTF-8");
		// LOGGER.debug("contenbt: " + theString);
		LOGGER.debug(
			"Used Proxy Address: " + httpResponse.getFirstHeader(ProxySwarm.PROXY_SWARM_HEADER).getValue());
	    } catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }

	    /*
	     * get my accessing IP from whatismyip.com
	     */
	    String inputLine;
	    BufferedReader br;
	    try {
		br = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
		while ((inputLine = br.readLine()) != null) {

		    Matcher matcher1 = Pattern.compile("<h1>Your IP Address is <span id=\"ip\">(.*)</span></h1>")
			    .matcher(inputLine);

		    if (matcher1.find()) {
			LOGGER.info("matcher1=" + matcher1.group(1));
		    }
		}
	    } catch (UnsupportedOperationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }

	    /*
	     * read limit header from stocktwits
	     */

	    Header[] rateLimitHeadersRemain = httpResponse.getHeaders("X-RateLimit-Remaining");
	    if (rateLimitHeadersRemain != null && rateLimitHeadersRemain.length > 0) {
		LOGGER.debug("rateLimitHeadersRemaining=" + rateLimitHeadersRemain[0]);
	    }

	    proxySwarm.getProxyStats();

	}

    }

}
